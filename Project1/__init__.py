import numpy as np
from matplotlib import pyplot as plt


def main():
    """
    REPOSITORY: https://AngeeelD@bitbucket.org/AngeeelD/deeplearningproject1.git

    AND gate Neural Network using a Perceptron model to solve
    Created by:
        Duran Reynoso Jose Angel
        Parrao Alcantará Manuel Sebastían
        Villaseñor Barragan Jaime
    :return: nothing

    Notes:
        To verify if solution works, the method "perceptron_sdg_plot"
        returns an array called "weight", this array should be implemented
        on "f(x) = <x, weight[:-1]> - weight[-1]" formula like:

        ------------------------------------------------------------------
        For 2 INPUTS and 6 EPOCHS we have:

        Input Truth Table:
        0 0
        0 1
        1 0
        1 1

        Weights:
        [ 3.  2.  4.]

        Validation:
        -1 * 3  +   -1 * 2  - 4 = sign(-9)  = -1
        -1 * 3  +   1 * 2   - 4 = sign(-5)  = -1
        1 * 3   +   -1 * 2  - 4 = sign(-3)  = -1
        1 * 3   +   1 * 2   - 4 = sign(1)   = 1

        ------------------------------------------------------------------
        For 3 INPUTS and 17 EPOCHS we have:

        Input Truth Table:
        0 0 0
        0 0 1
        0 1 0
        0 1 1
        1 0 0
        1 0 1
        1 1 0
        1 1 1

        Weights:
        [ 4.  3.  2.  8.]

        Validation:
        -1 * 4 	+ -1 * 3 	+ -1 * 2    - 8 = sign(-17) = -1
        -1 * 4 	+ -1 * 3 	+ 1 * 2     - 8 = sign(-13) = -1
        -1 * 4 	+ 1 * 3 	+ -1 * 2    - 8 = sign(-11) = -1
        -1 * 4 	+ 1 * 3 	+ 1 * 2     - 8 = sign(-7) 	= -1
        1 * 4 	+ -1 * 3 	+ -1 * 2    - 8 = sign(-9) 	= -1
        1 * 4 	+ -1 * 3 	+ 1 * 2     - 8 = sign(-5) 	= -1
        1 * 4 	+ 1 * 3 	+ -1 * 2    - 8 = sign(-2) 	= -1
        1 * 4 	+ 1 * 3 	+ 1 * 2     - 8 = sign(1) 	= 1

    """
    data_set, labels = create_data_set_for_and_gate(3)

    weights = perceptron_sdg_plot(data_set, labels, epochs=20, plot=True)

    check_weights(data_set, weights)
    # show_data_in_plot(data_set)
    pass


def check_weights(dataset, weigths, print_details=True):
    if print_details:
        print("\napplying f(x) = <x, weight[:-1]> - weight[-1] formula\n")
        for row in dataset:
            sum = 0
            for column_index in range(0, len(row) - 1):
                sum += (row[column_index] * weigths[column_index])
                print("{} * {}".format(row[column_index], weigths[column_index]), end="")
                if column_index != (len(row) - 2):
                    print(" + ", end="")
                else:
                    sum -= weigths[len(weigths) - 1]
                    print(" - {} = ".format(weigths[len(weigths) - 1]), end="")
            print("sign({}) = {}".format(sum, np.sign(sum)))
    pass


def create_data_set_for_and_gate(inputs=4):
    """
    Create a and gate truth table
    :param inputs: number of inputs
    :return: labeled numpy array
    """
    table = truth_table(inputs)
    for row in table:
        row.append(-1)
        pass
    labels = np.full(len(table), 1)
    labels[-1] = -1

    return np.array(table), np.array(labels)


def truth_table(n):
    """
    Create all variants of inputs on truth table
    :param n: number of inputs
    :return: array of inputs on all states
    """
    if n < 1:
        return [[]]
    subtable = truth_table(n - 1)
    return [row + [v] for row in subtable for v in [0, 1]]


def show_data_in_plot(data_set):
    """
    Shows a graph with data set data
    NOTE: Only works on 2 inputs data set
    :param data_set: labeled data set
    :return: nothigs
    """
    for d, sample in enumerate(data_set):
        if d < 3:
            # ploting the negative samples
            plt.scatter(sample[0], sample[1], s=120, marker="_", linewidths=2)
        else:
            # ploting the positive samples
            plt.scatter(sample[0], sample[1], s=120, marker="+", linewidths=2)

    # Print a possible hyperplane, that is seperating the two classes.
    plt.plot()
    plt.show()
    pass


def perceptron_sgd(x, y):
    """
    Trains a Perceptron model
    :param x: labeled data set
    :param y: correct labels
    :return: weight vector as a numpy array
    """
    weight = np.zeros(len(x[0]))
    eta = 1  # Learning Rate
    epochs = 20

    for epoch in range(epochs):
        for i, x in enumerate(x):
            if (np.dot(x[i], weight) * y[i]) <= 0:
                weight = weight + eta * x[i] * y[i]
    return weight


def perceptron_sdg_plot(data_set, labels, epochs=10, plot=True):
    """
        Trains perceptron model and plot the total loss in each epoch.
        :param epochs: number of epochs to train
        :param data_set: labeled data set
        :param labels: correct labels
        :return: weight vector as a numpy array
        """
    weight = np.zeros(len(data_set[0]))
    eta = 1  # Learning Rate
    errors = []

    for epoch in range(epochs):
        total_error = 0
        for i, ex in enumerate(data_set):
            product = np.dot(data_set[i], weight)
            if (product * labels[i]) <= 0:
                total_error += (np.dot(data_set[i], weight) * labels[i])
                weight = weight + eta * data_set[i] * labels[i]
        errors.append(total_error * -1)

    print("Trained weight of perceptron {}".format(weight))

    if plot:
        plt.plot(errors)
        plt.xlabel('Epochs')
        plt.xlabel('Total loss')
        plt.show()

    return weight


if __name__ == "__main__":
    main()
